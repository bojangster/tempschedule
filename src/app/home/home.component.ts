import { Component } from '@angular/core';

enum SlotState {
    None = 'none',
    Default = 'default',
    First = 'first',
    Middle = 'middle',
    Last = 'last'
}

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent {

    public data: Slot[];

    constructor() {
        this.data = [];
        for (let i = 0; i < 10; i++) {
            this.data.push({
                state: SlotState.None,
                valid: true,
                selected: false,
                marked: false
            });
        }
    }

    public editSlot(slot: Slot): void {
        const lastEditedSlot = this.data.find(x => x.selected);
        if (lastEditedSlot) {
            lastEditedSlot.selected = false;
        }
        slot.selected = true;
    }

    public submit(slot: Slot): void {
        console.log('submit values.');
        if (this.validate(slot)) {
            slot.selected = false;
            slot.marked = true;
        }
    }

    public clear(): void {
        this.data.forEach(x => x.marked = false);
    }

    public mark(slot: Slot): void {
        slot.marked = !slot.marked;
    }

    public hasMarkedSlots(): boolean {
        const markedCount =  this.data.filter(x => x.marked);
        if (markedCount && markedCount.length > 1) {
            return true;
        } else {
            return false;
        }
    }

    public merge(): void {
        // find first and last index of marked cells
        const marked = this.data.filter(x => x.marked);
        const first = marked[0];
        const fi = this.data.indexOf(first);
        first.state = SlotState.First;
        first.marked = false;
        const last = marked[marked.length - 1];
        last.state = SlotState.Last;
        last.marked = false;
        const li = this.data.indexOf(last);
        for (let index = fi + 1; index <= li; index++) {
            const element = this.data[index];
            element.min = first.min;
            element.max = first.max;
            element.price = first.price;
            element.marked = false;
            if (element.state === SlotState.None) {
                element.state = SlotState.Middle;
            }
        }
    }

    public validate(slot: Slot): boolean {
        console.log('--- validation --- ');
        slot.valid = !!slot.min && !!slot.max && !!slot.price;
        const regex = new RegExp(/\w/);
        if (slot.min != null) {
            const minv = regex.test(slot.min.toString());
            console.log('min: ' + minv);
        } else {
            slot.valid = false;
            return false;
        }
        if (slot.max != null) {
            const maxv = regex.test(slot.max.toString());
            console.log('max: ' + maxv);
        } else {
            slot.valid = false;
            return false;
        }
        if (slot.price != null) {
            const pricev = regex.test(slot.price.toString());
            console.log('price: ' + pricev);
        } else {
            slot.valid = false;
            return false;
        }
        console.log(slot.valid);
        return slot.valid;
    }

    public isValid(): boolean {
        return this.data.every(x => x.valid);
    }

    public save(): void {
        console.log('saving data...');
    }
}

interface Slot {
    min?: number;
    max?: number;
    price?: number;
    state: SlotState;
    valid: boolean;
    selected: boolean;
    marked: boolean;
}
